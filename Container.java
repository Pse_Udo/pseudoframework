package org.pseudo.api.framework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Container {

	private final List<Task> tasks;
	private final List<PrioritizedTask> prioritizedTasks;
	public Container() {
		tasks = Collections.synchronizedList(new ArrayList<Task>());
		prioritizedTasks = Collections.synchronizedList(new ArrayList<PrioritizedTask>());
	}
	
	public void submit(final Task... t) {
		for(final Task t1 : t) {
			if(t1 instanceof PrioritizedTask) {
				prioritizedTasks.add((PrioritizedTask) t1);
			} else {
				tasks.add(t1);
			}
		}
	}
	
	public void revoke(final Task... t) {
		tasks.removeAll(Arrays.asList(t));
	}
	
	public Task getNextAvailable() {
		return null;
	}
	
}
