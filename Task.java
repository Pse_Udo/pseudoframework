package org.pseudo.api.framework;

public interface Task {
	public void execute();
}
