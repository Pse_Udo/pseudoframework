package org.pseudo.api.framework;

public interface PrioritizedTask extends Task {
	public Priority getPriority();
}
