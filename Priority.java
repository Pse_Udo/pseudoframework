package org.pseudo.api.framework;

public enum Priority {
	VERY_HIGH(0),
	HIGH(1),
	NORMAL(2),
	LOW(3),
	VERY_LOW(4);
	private final int index;
	Priority(final int idx) {
		this.index = idx;
	}
	
	public int getIndex() {
		return index;
	}
}
